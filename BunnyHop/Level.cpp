#include "Level.h"
#include "Game.h"



Level::Level(Game* newGamePointer)
	: playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	, platformInstance()
{
	sf::Vector2f newPos;
	newPos.x = newGamePointer->GetWindow().getSize().x / 2;
	newPos.y = newGamePointer->GetWindow().getSize().y / 2;
		
	newPos.x -= platformInstance.GetHitbox().width / 2;
	newPos.y -= platformInstance.GetHitbox().height / 2;

	const float PLATFORM_OFFSET = 250;

	newPos.y += PLATFORM_OFFSET;

	platformInstance.SetPosition(newPos);

}

void Level::Input()
{
	playerInstance.Input();
}

void Level::Update(sf::Time frameTime)
{
	playerInstance.Update(frameTime);
	
}

void Level::DrawTo(sf::RenderTarget& target)
{
	playerInstance.DrawTo(target);
	platformInstance.DrawTo(target);
}
