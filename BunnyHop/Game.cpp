#include "Game.h"

Game::Game()
	: window(sf::VideoMode::getDesktopMode(), "Bunny Hop", sf::Style::Titlebar | sf::Style::Close)
	, gameClock()
	, LevelInstance(this)
{
	// Window Setup
	window.setMouseCursorVisible(false);
}

void Game::RunGameLoop() 
{
	// Repeat as long as the window is open
	while (window.isOpen())
	{
		Input();
		Update();
		Draw();
	}

}

void Game::Input()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}

		// Close game if escape is pressed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			window.close();
		}
	}

	LevelInstance.Input();

}

void Game::Update() 
{
	sf::Time frameTime = gameClock.restart();
	LevelInstance.Update(frameTime);
}

void Game::Draw() 
{
	window.clear();
	LevelInstance.DrawTo(window);
	window.display();
}

sf::RenderWindow& Game::GetWindow() 
{
	return window;
}


