#pragma once
#include "SpriteObject.h"

class Platform : public SpriteObject
{
public:
	Platform();

	void SetPosition(sf::Vector2f newPos);
};

