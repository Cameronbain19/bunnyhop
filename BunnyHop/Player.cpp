#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2u screenSize)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 75, 100, 8.0f)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, gravity(800.0f)
{
	AddClip("Jump", 0, 1);

	PlayClip("Jump");

	// Position the player at the centre of the screen
	sf::Vector2f newPosition;
	newPosition.x = ((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f;
	newPosition.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(newPosition);
}

void Player::Input()
{
	velocity.x = 0.0f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		velocity.x = speed;
	}
}

void Player::Update(sf::Time frameTime)
{
	velocity.y = velocity.y + gravity * frameTime.asSeconds();

	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	sprite.setPosition(newPosition);
	AnimatingObject::Update(frameTime);
}