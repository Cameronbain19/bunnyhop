#pragma once
#include <SFML/Graphics.hpp>
#include "Level.h"

class Game
{
public:
	Game();

	sf::RenderWindow& GetWindow();
	void RunGameLoop();

	void Input();
	void Update();
	void Draw();

private:
	sf::RenderWindow window;
	sf::Clock gameClock;
	Level LevelInstance;


};

