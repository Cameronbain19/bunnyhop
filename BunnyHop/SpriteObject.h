#pragma once
#include <SFML/Graphics.hpp>

class SpriteObject
{

public:
	// Constructors / Destructors
	SpriteObject(sf::Texture& newTexture);
	// Functions
	void DrawTo(sf::RenderTarget& target);

	sf::FloatRect GetHitbox();

protected:
	// Data
	sf::Sprite sprite;
	sf::Vector2f CalculateCollisionDepth(sf::FloatRect otherHitbox);

};