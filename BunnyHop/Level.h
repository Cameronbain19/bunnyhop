#pragma once
#include "Player.h"
#include "Platform.h"
#include <SFML/Graphics.hpp>

class Game;

class Level
{
public:
	Level(Game* newGamePointer);

	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

private:

	Game* gamePointer;
	Player playerInstance;
	Platform platformInstance;
};

