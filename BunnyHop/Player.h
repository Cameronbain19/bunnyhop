#pragma once
#include "AnimatingObject.h"

class Player : public AnimatingObject
{

public:
    Player(sf::Vector2u screenSize);

    void Input();
    void Update(sf::Time frameTime);

private:

    sf::Vector2f velocity;
    float speed;
    float gravity;

};

